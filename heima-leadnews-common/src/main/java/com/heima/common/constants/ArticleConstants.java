package com.heima.common.constants;

/**
 * @author 18297
 * @version 1.0
 * @project heima-leadnews
 * @date 2024/1/11 22:46:55
 */

public class ArticleConstants {
    public static final Short LOADTYPE_LOAD_MORE = 1;
    public static final Short LOADTYPE_LOAD_NEW = 2;
    public static final String DEFAULT_TAG = "__all__";

}