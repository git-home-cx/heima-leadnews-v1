package com.heima.common.config;


import com.baidu.aip.contentcensor.AipContentCensor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 百度SDK配置类
 *
 * @author liudo
 * @version 1.0
 * @project heima-leadnews
 * @date 2023/11/9 15:11:47
 */
@Configuration
@EnableConfigurationProperties(BaiduProperties.class)
public class BaiduConfiguration {

    /**
     * aip内容审查员
     *
     * @param baiduProperties 百度属性
     * @return {@link AipContentCensor}
     */
    @Bean
    public AipContentCensor aipContentCensor(BaiduProperties baiduProperties) {
        return new AipContentCensor(
                baiduProperties.getAppId(),
                baiduProperties.getApiKey(),
                baiduProperties.getSecretKey()
        );
    }
}
