package com.heima.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 百度SDK配置类
 *
 * @author liudo
 * @version 1.0
 * @project heima-leadnews
 * @date 2023/11/9 15:09:41
 */
@Data
@ConfigurationProperties(prefix = "baidu")
public class BaiduProperties {

    private String appId;

    private String apiKey;

    private String secretKey;

}
