package com.heima.feign.schedule;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.Task;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("leadnews-schedule")
public interface IScheduleClient {
    /**
     * @param task:
     * @return long
     * @author 18297
     * @date 2024/1/19 09:46
     * @description 添加延迟任务
     */
    @PostMapping("/api/v1/task/add")
    public ResponseResult<?> addTask(@RequestBody Task task);

    /**
     * @param taskId:
     * @return boolean
     * @author 18297
     * @date 2024/1/19 11:09
     * @description 取消任务
     */
    @GetMapping("/api/v1/task/cancel/{taskId}")
    public ResponseResult<?> cancelTask(@PathVariable("taskId") long taskId);

    /**
     * @param type:
     * @param priority:
     * @return Task
     * @author 18297
     * @date 2024/1/19 11:45
     * @description 按照类型和优先级拉取任务
     */
    @GetMapping("/api/v1/task/poll/{type}/{priority}")
    public ResponseResult<?> poll(@PathVariable("type") int type,@PathVariable("priority")  int priority);
}
