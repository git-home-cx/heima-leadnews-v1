package com.heima.feign.article.impl;

import com.heima.feign.article.IArticleClient;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.stereotype.Component;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/20 15:17:11
 */
@Component
public class ArticleClientFallback implements IArticleClient {
    /**
     * @param dto :
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/18 15:29
     * @description 新增或修改app文章信息
     */
    @Override
    public ResponseResult<?> saveArticle(ArticleDto dto) {
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR, "服务器正忙");
    }
}
