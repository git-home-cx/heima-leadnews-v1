package com.heima.feign.article;

import com.heima.feign.article.impl.ArticleClientFallback;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

//@FeignClient(value = "leadnews-article",fallback = ArticleClientFallback.class)
@FeignClient("leadnews-article")
public interface IArticleClient {
    /**
     * @author 18297
     * @date 2024/1/18 15:29
     * @description 新增或修改app文章信息
     * @param dto:
     * @return ResponseResult<?>
     */
    @PostMapping("/api/v1/article/saveOrUpdate")
    ResponseResult<?> saveArticle(@RequestBody ArticleDto dto);
}
