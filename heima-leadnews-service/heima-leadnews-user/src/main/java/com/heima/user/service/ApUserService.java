package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.pojos.ApUser;

public interface ApUserService extends IService<ApUser> {
    /**
     * @author 18297
     * @date 2024/1/12 22:05
     * @description 登录功能
     * @param loginDto:
     * @return ResponseResult<?>
     */
    ResponseResult<?> login(LoginDto loginDto);
}
