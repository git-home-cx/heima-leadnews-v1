package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserService;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.common.MD5Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/12 22:02:42
 */
@Slf4j
@Service
@Transactional
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {

    /**
     * @param loginDto :
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/12 22:05
     * @description app端登录功能
     */
    @Override
    public ResponseResult<?> login(LoginDto loginDto) {

        // 1.正常登录，校验参数，用户名和密码
        if (StringUtils.isNotBlank(loginDto.getPhone()) && StringUtils.isNotBlank(loginDto.getPassword())) {
            // 1.1.根据手机号从数据库查询用户
            ApUser dbUser = getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, loginDto.getPhone()));
            // 1.2.如果查出来的用户为空，返回错误提示
            if (Objects.isNull(dbUser)) {
                return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST, "用户信息不存在");
            }
            // 获取当前用户的salt加上前端传来的密码，组合进行md5加密
            String salt = dbUser.getSalt();
            String pwd = loginDto.getPassword();
            String pwdSalt = DigestUtils.md5DigestAsHex((pwd + salt).getBytes());
            // 获取用户数据库中的密码，进行对比
            String password = dbUser.getPassword();
            // 不相等返回错误提示
            if (!password.equals(pwdSalt)) {
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR, "用户名/密码有误");
            }
            // 密码正确，根据用户id生成JWT
            String token = AppJwtUtil.getToken(dbUser.getId().longValue());
            // 创建map集合封装用户信息返回
            Map<String, Object> map = new HashMap<>();
            // salt和password不返回，置空
            dbUser.setPassword(null);
            dbUser.setSalt(null);
            map.put("token", token);
            map.put("user", dbUser);
            return ResponseResult.okResult(map);
        } else {
            //手机号，密码为空 游客登录
            Map<String, Object> map = new HashMap<>();
            String token = AppJwtUtil.getToken(0L);
            map.put("token", token);
            return ResponseResult.okResult(map);
        }

    }
}
