package com.heima.schedule.feign;

import com.heima.feign.schedule.IScheduleClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.Task;
import com.heima.schedule.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/19 16:28:25
 */
@RestController
public class ScheduleClient implements IScheduleClient {
    @Autowired
    private TaskService taskService;
    /**
     * @param task :
     * @return long
     * @author 18297
     * @date 2024/1/19 09:46
     * @description 添加延迟任务
     */
    @PostMapping("/api/v1/task/add")
    @Override
    public ResponseResult<?> addTask(@RequestBody Task task) {
        return ResponseResult.okResult(taskService.addTask(task));
    }

    /**
     * @param taskId :
     * @return boolean
     * @author 18297
     * @date 2024/1/19 11:09
     * @description 取消任务
     */
    @GetMapping("/api/v1/task/cancel/{taskId}")
    @Override
    public ResponseResult<?> cancelTask(@PathVariable("taskId") long taskId) {
        return ResponseResult.okResult(taskService.cancelTask(taskId));
    }

    /**
     * @param type     :
     * @param priority :
     * @return Task
     * @author 18297
     * @date 2024/1/19 11:45
     * @description 按照类型和优先级拉取任务
     */
    @GetMapping("/api/v1/task/poll/{type}/{priority}")
    @Override
    public ResponseResult<?> poll(@PathVariable("type") int type, @PathVariable("priority") int priority) {
        return ResponseResult.okResult(taskService.poll(type,priority));
    }
}
