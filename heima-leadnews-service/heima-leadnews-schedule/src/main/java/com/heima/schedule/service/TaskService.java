package com.heima.schedule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.schedule.dtos.Task;

public interface TaskService{
    /**
     * @param task:
     * @return long
     * @author 18297
     * @date 2024/1/19 09:46
     * @description 添加延迟任务
     */
    long addTask(Task task);

    /**
     * @param taskId:
     * @return boolean
     * @author 18297
     * @date 2024/1/19 11:09
     * @description 取消任务
     */
    boolean cancelTask(long taskId);

    /**
     * @param type:
     * @param priority:
     * @return Task
     * @author 18297
     * @date 2024/1/19 11:45
     * @description 按照类型和优先级拉取任务
     */
    Task poll(int type, int priority);
}
