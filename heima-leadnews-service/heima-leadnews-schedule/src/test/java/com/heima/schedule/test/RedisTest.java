package com.heima.schedule.test;

import com.heima.common.redis.CacheService;
import com.heima.schedule.ScheduleApplication;
import com.heima.schedule.service.TaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/18 23:00:34
 */
@SpringBootTest(classes = ScheduleApplication.class)
@RunWith(SpringRunner.class)
public class RedisTest {
    @Autowired
    private CacheService cacheService;
    @Test
    public void testList() {
/*//         从左向list添加一条数据
        cacheService.lLeftPush("list_001","hello,redis");
//         从list右边获取一条数据
        String list001 = cacheService.lRightPop("list_001");
        System.out.println(list001);*/
    }

    @Test
    public void testZset() {
        // 添加数据到zset中
/*        cacheService.zAdd("zset_key_001","zset1号",1000);
        cacheService.zAdd("zset_key_001","zset2号",999);
        cacheService.zAdd("zset_key_001","zset3号",888);
        cacheService.zAdd("zset_key_001","zset4号",777);*/
        //按照分值获取数据
        Set<String> strings = cacheService.zRangeByScore("zset_key_001", 0, 999);
        System.out.println(strings);
    }

}
