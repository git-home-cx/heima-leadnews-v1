package com.heima.article.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ApArticleService;
import com.heima.article.service.FreemarkerService;
import com.heima.common.constants.ArticleConstants;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.common.dtos.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/13 11:49:16
 */
@Slf4j
@Service
@Transactional
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle> implements ApArticleService {
    @Autowired
    private ApArticleMapper apArticleMapper;
    @Autowired
    private ApArticleContentMapper apArticleContentMapper;
    @Autowired
    private ApArticleConfigMapper apArticleConfigMapper;
    @Autowired
    private FreemarkerService freemarkerService;

    /**
     * @param dto:
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/18 17:16
     * @description 新增或修改自媒体端传来的文章
     */
    @Override
    public ResponseResult<?> saveOrUpdateArticle(ArticleDto dto) {
        ApArticle apArticle = new ApArticle();
        BeanUtils.copyProperties(dto, apArticle);
        // 判断有没有id
        if (dto.getId() == null) {
            //没有就新增 到文章表
            apArticleMapper.insert(apArticle);
            // 给dto加上id，后面生成详情页时使用
            dto.setId(apArticle.getId());
            // 添加文章配置表信息
            ApArticleConfig config = new ApArticleConfig(apArticle.getId());
            apArticleConfigMapper.insert(config);
            // 添加文章内容表信息
            ApArticleContent apArticleContent = new ApArticleContent();
            apArticleContent.setContent(dto.getContent());
            apArticleContent.setArticleId(apArticle.getId());
            apArticleContentMapper.insert(apArticleContent);
        } else {
            //有id就修改
            apArticleMapper.updateById(apArticle);
            ApArticleContent apArticleContent = apArticleContentMapper.selectOne(Wrappers.<ApArticleContent>lambdaQuery()
                    .eq(ApArticleContent::getArticleId, dto.getId()));
            apArticleContent.setContent(dto.getContent());
            apArticleContentMapper.updateById(apArticleContent);
        }
        freemarkerService.buildPage(dto);
        // 将文章id返回给调用者
        return ResponseResult.okResult(String.valueOf(apArticle.getId()));
    }

    /**
     * @param dto  :
     * @param type :1为加载更多  2为加载最新
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/13 11:48
     * @description 根据参数加载文章列表
     */
    @Override
    public ResponseResult<?> load(ArticleHomeDto dto, Short type) {
        // 参数校验
        // 分页参数校验
        Integer size = dto.getSize();
        if (size == null || size == 0) {
            // size为空为0就设置size为10
            size = 10;
        }
        // size最大不超50
        size = Math.min(size, 50);
        dto.setSize(size);

        // 参数type校验
        if (!type.equals(ArticleConstants.LOADTYPE_LOAD_MORE) && !type.equals(ArticleConstants.LOADTYPE_LOAD_NEW)) {
            // type默认设置为1
            type = ArticleConstants.LOADTYPE_LOAD_MORE;
        }

        // 频道参数校验
        if (StringUtils.isBlank(dto.getTag())) {
            // 设置频道参数默认为__all__
            dto.setTag(ArticleConstants.DEFAULT_TAG);
        }
        // 时间校验
        if (dto.getMaxBehotTime() == null) {
            dto.setMaxBehotTime(new Date());
        }

        if (dto.getMinBehotTime() == null) {
            dto.setMinBehotTime(new Date());
        }

        List<ApArticle> apArticles = apArticleMapper.loadArticleList(dto, type);
        return ResponseResult.okResult(apArticles);
    }

}
