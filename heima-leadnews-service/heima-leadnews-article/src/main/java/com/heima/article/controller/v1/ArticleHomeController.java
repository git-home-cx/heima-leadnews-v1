package com.heima.article.controller.v1;

import com.heima.article.service.ApArticleService;
import com.heima.common.constants.ArticleConstants;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.common.dtos.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/13 11:29:22
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/article")
public class ArticleHomeController {

@Autowired
private ApArticleService apArticleService;
    /**
     * @author 18297
     * @date 2024/1/13 11:33
     * @description 加载文章列表
     * @param dto:
     * @return ResponseResult<?>
     */
    @PostMapping("/load")
    public ResponseResult<?> load(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleConstants.LOADTYPE_LOAD_MORE);
    }

    /**
     * @author 18297
     * @date 2024/1/13 11:34
     * @description 上拉加载更多文章
     * @param dto:
     * @return ResponseResult<?>
     */
    @PostMapping("/loadmore")
    public ResponseResult<?> loadMore(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleConstants.LOADTYPE_LOAD_MORE);
    }
    /**
     * @author 18297
     * @date 2024/1/13 11:35
     * @description 下拉加载最新文章
     * @param dto:
     * @return ResponseResult<?>
     */
    @PostMapping("/loadnew")
    public ResponseResult<?> loadNew(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleConstants.LOADTYPE_LOAD_NEW);
    }
}
