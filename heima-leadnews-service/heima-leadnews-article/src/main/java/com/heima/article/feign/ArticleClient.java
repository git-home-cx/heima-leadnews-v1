package com.heima.article.feign;

import com.heima.article.service.ApArticleService;
import com.heima.feign.article.IArticleClient;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/18 15:32:00
 */
@RestController
public class ArticleClient implements IArticleClient {
    @Autowired
    private ApArticleService apArticleService;
    /**
     * @param dto :
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/18 15:29
     * @description 新增或修改app文章信息
     */
    @Override
    public ResponseResult<?> saveArticle(ArticleDto dto) {
        return apArticleService.saveOrUpdateArticle(dto);
    }
}
