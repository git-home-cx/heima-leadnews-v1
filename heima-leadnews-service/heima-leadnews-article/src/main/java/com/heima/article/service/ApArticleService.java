package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

public interface ApArticleService extends IService<ApArticle> {
    /**
     * @author 18297
     * @date 2024/1/13 11:48
     * @description 根据参数加载文章列表
     * @param dto:
     * @param type:1为加载更多  2为加载最新
     * @return ResponseResult<?>
     */
    ResponseResult<?> load(ArticleHomeDto dto, Short type);

    ResponseResult<?> saveOrUpdateArticle(ArticleDto dto);
}
