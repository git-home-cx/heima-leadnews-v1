package com.heima.article.service;

import com.heima.model.article.dtos.ArticleDto;

public interface FreemarkerService {
    void buildPage(ArticleDto articleDto);
}
