package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {
    /**
     * @author 18297
     * @date 2024/1/13 11:41
     * @description 查询文章列表
     * @param dto:
     * @param type: 1 加载更多文章 2 加载最新文章
     * @return List<ApArticle>
     */
    public List<ApArticle> loadArticleList(ArticleHomeDto dto, short type);
}
