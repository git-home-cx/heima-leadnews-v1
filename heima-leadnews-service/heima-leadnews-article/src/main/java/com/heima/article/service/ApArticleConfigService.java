package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.pojos.ApArticleConfig;

import java.util.Map;

public interface ApArticleConfigService extends IService<ApArticleConfig> {
    /**
     * @author 18297
     * @date 2024/1/21 20:26
     * @description 修改文章上下架状态
     * @param map:
     * @return void
     */
    void updateByMap(Map map);
}
