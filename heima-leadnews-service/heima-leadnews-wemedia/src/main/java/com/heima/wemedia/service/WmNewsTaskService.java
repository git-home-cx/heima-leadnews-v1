package com.heima.wemedia.service;

import java.util.Date;

public interface WmNewsTaskService {

    /**
     * @param id:          文章的id
     * @param publishTime: 发布时间，作为任务的执行时间
     * @author 18297
     * @date 2024/1/19 16:37
     * @description 添加任务到延迟队列
     */
    public void addNewsToTask(Integer id, Date publishTime);

    /**
     * @author 18297
     * @date 2024/1/19 17:32
     * @description 消费任务，审核文章
     */
    public void scanNewsByTask();
}
