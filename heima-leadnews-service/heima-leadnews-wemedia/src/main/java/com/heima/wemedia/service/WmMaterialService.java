package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

public interface WmMaterialService extends IService<WmMaterial> {
    /**
     * @param multipartFile:
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/14 21:58
     * @description 图片上传
     */
    ResponseResult<?> uploadPicture(MultipartFile multipartFile);

    /**
     * @param dto:
     * @return ResponseResult
     * @author 18297
     * @date 2024/1/14 22:58
     * @description 查看素材列表
     */
    ResponseResult<?> findList(WmMaterialDto dto);

    /**
     * @author 18297
     * @date 2024/1/15 21:59
     * @description 根据id删除图片
     * @param id:
     * @return ResponseResult<?>
     */
    ResponseResult<?> deletePicture(Integer id);

    /**
     * @author 18297
     * @date 2024/1/15 22:53
     * @description 收藏图片
     * @param id:
     * @return ResponseResult<?>
     */
    ResponseResult<?> Collect(Integer id);

    /**
     * @author 18297
     * @date 2024/1/16 15:44
     * @description 取消收藏
     * @param id:
     * @return ResponseResult<?>
     */
    ResponseResult<?> cancelCollect(Integer id);
}