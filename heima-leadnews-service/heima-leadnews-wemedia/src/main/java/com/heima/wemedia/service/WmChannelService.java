package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;

public interface WmChannelService extends IService<WmChannel> {
    /**
     * @author 18297
     * @date 2024/1/15 14:48
     * @description 查询所有频道
     * @param :
     * @return ResponseResult<?>
     */
    ResponseResult<?> findAll();

}