package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/15 14:44:27
 */
@RestController
@RequestMapping("/api/v1/channel")
public class WmChannelController {
    @Autowired
    private WmChannelService wmChannelService;
    /**
     * @author 18297
     * @date 2024/1/20 17:29
     * @description 查询频道列表
     * @param :
     * @return ResponseResult<?>
     */
    @GetMapping("/channels")
    public ResponseResult<?> findAll(){
        return wmChannelService.findAll();
    }
}
