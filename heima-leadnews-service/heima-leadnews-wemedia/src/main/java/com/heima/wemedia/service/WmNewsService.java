package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;

public interface WmNewsService extends IService<WmNews> {
    /**
     * @param dto:
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/15 15:34
     * @description 条件查询文章列表
     */
    ResponseResult<?> findList(WmNewsPageReqDto dto);

    /**
     * @param wmNewsDto:
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/16 16:30
     * @description 发布修改的文章或保存为草稿
     */
    ResponseResult<?> submitNews(WmNewsDto wmNewsDto);

    /**
     * @param id:
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/16 19:20
     * @description 查看文章详情
     */
    ResponseResult<?> findOne(Integer id);

    /**
     * @param dto:
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/16 19:24
     * @description 文章上下架
     */
    ResponseResult<?> downOrUp(WmNewsDto dto);

    /**
     * @param id:
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/16 19:32
     * @description 删除文章
     */
    ResponseResult<?> deleteNews(Integer id);
}
