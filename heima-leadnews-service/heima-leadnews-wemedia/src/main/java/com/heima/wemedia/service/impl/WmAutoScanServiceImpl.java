package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.heima.common.exception.CustomException;
import com.heima.feign.article.IArticleClient;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmSensitive;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.common.SensitiveWordUtil;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmSensitiveMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.BaiduScanService;
import com.heima.wemedia.service.WmAutoScanService;
import net.sourceforge.tess4j.Tesseract;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/18 13:33:17
 */
@Service
public class WmAutoScanServiceImpl implements WmAutoScanService {
    @Autowired
    private WmNewsMapper wmNewsMapper;
    @Autowired
    private IArticleClient iArticleClient;
    @Autowired
    private WmUserMapper wmUserMapper;
    @Autowired
    private WmChannelMapper wmChannelMapper;
    @Autowired
    private BaiduScanService baiduScanService;
    @Autowired
    private WmSensitiveMapper wmSensitiveMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private Tesseract tesseract;

    @Override
    @Async
    public void autoScan(Integer newId) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 1.根据文章id校验文章信息，a 文章是否存在 b 文章是否处于审核状态
        WmNews wmNews = wmNewsMapper.selectById(newId);
        if (wmNews == null || wmNews.getStatus() != WmNews.Status.SUBMIT.getCode()) {
            throw new CustomException(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        // 2.提取文章中所有的文本图片
        Map<String, Object> textAndImg = handleTextAndImg(wmNews);

        // 3. 审核文本
        boolean isTextScan = scanText((String) textAndImg.get("text"), wmNews);
        if (!isTextScan) {
            return;
        }

        // 4.再审核图片
        boolean isImgScan = scanImg((HashSet<String>) textAndImg.get("img"), wmNews);
        if (!isImgScan) {
            return;
        }
        //5.远程调用article微服务，将自媒体文章信息wm_news同步到表中
        saveArticle(wmNews);
    }

    private void saveArticle(WmNews wmNews) {
        // 调用feign接口，将数据传给article微服务进行保存
        ArticleDto articleDto = new ArticleDto();
        BeanUtils.copyProperties(wmNews, articleDto);
        // 以下字段需要单独处理
        //app文章id：articleId
        Long aid = wmNews.getArticleId();
        if (aid != null) {
            //说明当前文章之前同步过，本次属于修改操作
            articleDto.setId(aid);
        }

        // 作者id
        articleDto.setAuthorId(wmNews.getUserId().longValue());
        // 作者名字
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());
        if (wmUser != null) {
            articleDto.setAuthorName(wmUser.getName());
        }
        // 频道名字
        WmChannel wmChannel = wmChannelMapper.selectById(wmNews.getChannelId());
        if (wmChannel != null) {
            articleDto.setChannelName(wmChannel.getName());
        }

        // 文章布局
        articleDto.setLayout(wmNews.getType());
        //调用feign接口，将数据传递给article为服务进行保存
        ResponseResult result = iArticleClient.saveArticle(articleDto);

        if (result.getCode() == 200) {
            //同步app文章数据成功,切记要回填：article_id字段到wn_news表中
            String articleId = result.getData().toString();
            wmNews.setArticleId(Long.valueOf(articleId));
            updateNewsStatus(wmNews, WmNews.Status.PUBLISHED, "审核成功");
        }
    }

    /**
     * @author 18297
     * @date 2024/1/20 11:30
     * @description 审核图片
     * @param img:
     * @param wmNews:
     * @return boolean
     */
    private boolean scanImg(HashSet<String> img, WmNews wmNews) {
        for (String url : img) {
            // 使用tess4j进行OCR识别，将图片中的文字提取出来进行文本审核
            byte[] bytes = fileStorageService.downLoadFile(url);
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            BufferedImage bufferedImage = null;
            try {
                bufferedImage= ImageIO.read(bis);
                String textInImg = tesseract.doOCR(bufferedImage);
                boolean flag = scanText(textInImg, wmNews);
                if (!flag) {
                    return flag;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Map<String, Object> resMap = baiduScanService.scanImg(url);
            int conclusionType = Integer.parseInt(resMap.get("conclusionType").toString());
            if (conclusionType == 2 || conclusionType == 4) {
                // 不合规，修改文章状态：2失败
                updateNewsStatus(wmNews, WmNews.Status.FAIL, "图片内容不合规");
                return false;
            } else if (conclusionType == 3) {
                // 疑似不合规，修改文章状态：3 等待人工审核
                updateNewsStatus(wmNews, WmNews.Status.ADMIN_AUTH, "图片内容疑似不合规");
                return false;
            }
        }
        return true;
    }


    private void updateNewsStatus(WmNews wmNews, WmNews.Status fail, String reason) {
        wmNews.setStatus(fail.getCode());
        wmNews.setReason(reason);
        wmNewsMapper.updateById(wmNews);
    }

    private boolean scanText(String text, WmNews wmNews) {
        //热点数据，访问频次高
        // 先内审
        // 先查看缓存
        String sensitivesJSON = stringRedisTemplate.opsForValue().get("sensitives");
        if (StringUtils.isEmpty(sensitivesJSON)) {
            // 缓存没有，再查询数据库
            List<WmSensitive> wmSensitives = wmSensitiveMapper.selectList(null);
            List<String> list = wmSensitives.stream().map(WmSensitive::getSensitives).collect(Collectors.toList());
            // 把数据库查到的list转为json存到缓存中
            sensitivesJSON = JSON.toJSONString(list);
            stringRedisTemplate.opsForValue().set("sensitives", sensitivesJSON);
        }
        // 有的话，把缓存中的json反序列化为list
        List list = JSON.parseObject(sensitivesJSON, List.class);
        //初始化敏感词词典
        SensitiveWordUtil.initMap(list);
        // 基于词典，来匹配文章中是否有敏感词
        Map<String, Integer> map = SensitiveWordUtil.matchWords(text);
        if (!map.isEmpty()) {
            updateNewsStatus(wmNews, WmNews.Status.FAIL, "审核失败，内容违规");
            return false;
        }
        // 再外审--调用百度云接口
        Map<String, Object> resMap = baiduScanService.scanText(text);
        int conclusionType = Integer.parseInt(resMap.get("conclusionType").toString());
        if (conclusionType == 2 || conclusionType == 4) {
            // 不合规，修改文章状态：2失败
            updateNewsStatus(wmNews, WmNews.Status.FAIL, "文章内容不合规");
            return false;
        } else if (conclusionType == 3) {
            // 疑似不合规，修改文章状态：3 等待人工审核
            updateNewsStatus(wmNews, WmNews.Status.ADMIN_AUTH, "文章内容疑似不合规");
            return false;
        }
        return true;
    }

    private Map<String, Object> handleTextAndImg(WmNews wmNews) {
        // 用来收集文章中所有的文本内容
        StringBuilder textBox = new StringBuilder(wmNews.getTitle());
        // 用来收集文章中所有的图片内容
        HashSet<String> imgBox = new HashSet<>();

        // 处理文章内容部分
        String contentStr = wmNews.getContent();
        JSONArray content = JSON.parseArray(contentStr);
        for (Object o : content) {
            JSONObject jsonObject = JSONObject.parseObject(o.toString());
            if ("text".equals(jsonObject.getString("type"))) {
                textBox.append(jsonObject.getString("value"));
            } else {
                imgBox.add(jsonObject.getString("value"));
            }
        }

        // 处理文章标签内容
        textBox.append(wmNews.getLabels());
        Map<String, Object> res = new HashMap<>();
        res.put("text", textBox.toString());
        res.put("img", imgBox);
        return res;
    }
}
