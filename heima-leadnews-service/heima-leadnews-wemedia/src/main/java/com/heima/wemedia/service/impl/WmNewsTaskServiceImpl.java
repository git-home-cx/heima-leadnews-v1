package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.feign.schedule.IScheduleClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.TaskTypeEnum;
import com.heima.model.schedule.dtos.Task;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.utils.common.ProtostuffUtil;
import com.heima.wemedia.service.WmAutoScanService;
import com.heima.wemedia.service.WmNewsTaskService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/19 16:38:27
 */
@Slf4j
@Service
public class WmNewsTaskServiceImpl implements WmNewsTaskService {
    @Autowired
    private IScheduleClient scheduleClient;
    @Autowired
    private WmAutoScanService wmAutoScanService;

    /**
     * @param id          :          文章的id
     * @param publishTime : 发布时间，作为任务的执行时间
     * @author 18297
     * @date 2024/1/19 16:37
     * @description 添加任务到延迟队列
     */
    @Override
    @Async
    public void addNewsToTask(Integer id, Date publishTime) {

        log.info("添加任务到延迟服务中----begin");
        Task task = new Task();
        task.setExecuteTime(publishTime.getTime());
        task.setTaskType(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType());
        task.setPriority(TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
        WmNews wmNews = new WmNews();
        wmNews.setId(id);
        task.setParameters(ProtostuffUtil.serialize(wmNews));

        scheduleClient.addTask(task);

        log.info("添加任务到延迟服务中----end");

    }

    /**
     * @author 18297
     * @date 2024/1/19 17:32
     * @description 消费任务，审核文章
     */
    @Scheduled(fixedRate = 1000) // 每一秒去拉取一次redis的list集合中的任务去执行
    @SneakyThrows
    @Override
    public void scanNewsByTask() {


        ResponseResult responseResult = scheduleClient.poll(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType(), TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
        if(responseResult.getCode().equals(200) && responseResult.getData() != null){
            log.info("文章审核---消费任务执行---begin---");
            // responseResult.getData()返回的是一个JSON对象，如果直接强转为Task对象不安全，
            // 所以先转换JSONStr，再转为Task对象
            String json_str = JSON.toJSONString(responseResult.getData());
            Task task = JSON.parseObject(json_str, Task.class);
            // 获取任务参数
            byte[] parameters = task.getParameters();
            // 将参数反序列化为wmNews对象
            WmNews wmNews = ProtostuffUtil.deserialize(parameters, WmNews.class);
            System.out.println(wmNews.getId()+"-----------");
            wmAutoScanService.autoScan(wmNews.getId());
            log.info("文章审核---消费任务执行---end---");
        }
    }
}

