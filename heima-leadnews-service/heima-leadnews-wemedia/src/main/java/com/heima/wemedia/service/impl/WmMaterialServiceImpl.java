package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;


@Slf4j
@Service
@Transactional
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService {
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private WmMaterialMapper wmMaterialMapper;

    /**
     * @param multipartFile :
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/14 21:58
     * @description 图片上传
     */
    @Override
    public ResponseResult<?> uploadPicture(MultipartFile multipartFile) {
        //1. 检查参数
        if (multipartFile == null || multipartFile.getSize() == 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2. 上传图片到minIO中
        //生成uuid作为文件前缀
        String fileName = UUID.randomUUID().toString().replace("-", "");
        //获取文件后缀
        String originalFilename = multipartFile.getOriginalFilename();
        String postfix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String url = null;
        // 上传到minio返回url
        try {
            url = fileStorageService.uploadImgFile("", fileName + postfix, multipartFile.getInputStream());
            log.info("上传图片到MinIO中，fileId:{}", url);
        } catch (IOException e) {
            log.error("上传图片失败");
            e.printStackTrace();
        }

        //3. 保存到数据库中
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setUserId(WmThreadLocalUtil.getUser().getId());//用户id
        wmMaterial.setUrl(url); //返回的url
        wmMaterial.setIsCollection((short) 0); // 默认为收藏
        wmMaterial.setType((short) 0); // 类型为图片
        wmMaterial.setCreatedTime(new Date()); // 创建时间
        save(wmMaterial);
        //4. 返回结果
        return ResponseResult.okResult(wmMaterial);
    }

    /**
     * @param dto :
     * @return ResponseResult
     * @author 18297
     * @date 2024/1/14 22:58
     * @description 查看素材列表
     */
    @Override
    public ResponseResult<?> findList(WmMaterialDto dto) {
        // 1. 检查参数
        dto.checkParam();
        // 2. 分页查询
        IPage page = new Page<>(dto.getPage(), dto.getSize());
        // 添加查询条件
        LambdaQueryWrapper<WmMaterial> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 是否收藏
        if (dto.getIsCollection() != null && dto.getIsCollection() == 1) {
            lambdaQueryWrapper.eq(WmMaterial::getIsCollection, dto.getIsCollection());
        }
        // 按照用户查询
        lambdaQueryWrapper.eq(WmMaterial::getUserId, WmThreadLocalUtil.getUser().getId());
        // 按照时间倒序
        lambdaQueryWrapper.orderByDesc(WmMaterial::getCreatedTime);
        page = page(page, lambdaQueryWrapper);
        // 3. 返回数据
        ResponseResult responseResult = new PageResponseResult(dto.getPage(), dto.getSize(), (int) page.getTotal());
        responseResult.setData(page.getRecords());
        return responseResult;
    }

    /**
     * @param id :
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/15 21:59
     * @description 根据id删除图片
     */
    @Override
    public ResponseResult<?> deletePicture(Integer id) {
        if (id == null) {
            // 参数为空返回501
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 根据id查询素材表
        WmMaterial wmMaterial = getOne(Wrappers.<WmMaterial>lambdaQuery().eq(WmMaterial::getId, id));
        if (wmMaterial == null) {
            // 为空返回1002
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        // 根据id获取图片素材与文章关联信息
        WmNewsMaterial wmNewsMaterial = wmMaterialMapper.select(id);
        // 判断是否为空，不为空有文章关联返回提示
        if (Objects.nonNull(wmNewsMaterial)) {
            // 返回文件删除失败
            return ResponseResult.errorResult(AppHttpCodeEnum.RELATED_TO_ARTICLES);
        }
        // 无文章关联删除素材
        removeById(id);
        // 删除minio中的图片
        fileStorageService.delete(wmMaterial.getUrl());
        return ResponseResult.okResult(200,"操作成功");
    }

    /**
     * @param id :
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/15 22:53
     * @description 收藏图片
     */
    @Override
    public ResponseResult<?> Collect(Integer id) {
        if (id == null) {
            // 参数为空返回501
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 根据id获取对象
        WmMaterial wmMaterial = getOne(Wrappers.<WmMaterial>lambdaQuery().eq(WmMaterial::getId, id));
        if (wmMaterial == null) {
            // 对象为空返回501
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 修改收藏状态为已收藏
        wmMaterial.setIsCollection((short)1);
        // 更新数据库信息
        updateById(wmMaterial);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * @param id :
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/16 15:44
     * @description 取消收藏
     */
    @Override
    public ResponseResult<?> cancelCollect(Integer id) {
        if (id == null) {
            // 参数为空返回501
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 根据id获取对象
        WmMaterial wmMaterial = getOne(Wrappers.<WmMaterial>lambdaQuery().eq(WmMaterial::getId, id));
        if (wmMaterial == null) {
            // 对象为空返回501
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 修改收藏状态为已收藏
        wmMaterial.setIsCollection((short)0);
        // 更新数据库信息
        updateById(wmMaterial);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
