package com.heima.wemedia.service;

import java.util.Map;

public interface BaiduScanService {
    /**
     * @author 18297
     * @date 2024/1/18 10:32
     * @description 百度云审核-文本
     * @param text:
     * @return Map<String,Object>
     */
    Map<String,Object> scanText(String text);

    /**
     * @author 18297
     * @date 2024/1/18 10:33
     * @description 百度云审核-图片
     * @param imgUrl:
     * @return Map<String,Object>
     */
    Map<String,Object> scanImg(String imgUrl);
}
