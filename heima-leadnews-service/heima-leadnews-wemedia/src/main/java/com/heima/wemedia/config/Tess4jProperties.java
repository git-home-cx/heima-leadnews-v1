package com.heima.wemedia.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/20 11:34:01
 */
@ConfigurationProperties(prefix = "tess4j")
@Data
@Component
public class Tess4jProperties {
    private String dataPath;
    private String language;
}
