package com.heima.wemedia.controller.v1;

import com.baomidou.mybatisplus.extension.api.R;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/15 15:29:02
 */
@RestController
@RequestMapping("/api/v1/news")
public class WmNewsController {
    @Autowired
    private WmNewsService wmNewsService;
    /**
     * @author 18297
     * @date 2024/1/20 17:33
     * @description 查看文章内容列表
     * @param dto:
     * @return ResponseResult<?>
     */
    @PostMapping("/list")
    public ResponseResult<?> findList(@RequestBody WmNewsPageReqDto dto) {
        ResponseResult<?> list = wmNewsService.findList(dto);
        return list;
    }

    /**
     * @author 18297
     * @date 2024/1/20 17:33
     * @description 提交文章（重点）
     * @param wmNewsDto:
     * @return ResponseResult<?>
     */
    @PostMapping("/submit")
    public ResponseResult<?> submitNews(@RequestBody WmNewsDto wmNewsDto){
        return wmNewsService.submitNews(wmNewsDto);
    }

    /**
     * @author 18297
     * @date 2024/1/20 17:33
     * @description 根据id查询文章（修改文章回显）
     * @param id:
     * @return ResponseResult<?>
     */
    @GetMapping("/one/{id}")
    public ResponseResult<?> findOne(@PathVariable Integer id){
        return wmNewsService.findOne(id);
    }

    @GetMapping("/del_news/{id}")
    public ResponseResult<?> deleteNews(@PathVariable Integer id){
        return wmNewsService.deleteNews(id);
    }


    @PostMapping("/down_or_up")
    public ResponseResult<?> downOrUp(@RequestBody WmNewsDto dto){
        return wmNewsService.downOrUp(dto);
    }


}
