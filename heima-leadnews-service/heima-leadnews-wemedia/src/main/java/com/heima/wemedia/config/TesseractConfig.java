package com.heima.wemedia.config;

import net.sourceforge.tess4j.Tesseract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/20 11:36:16
 */
@Configuration
public class TesseractConfig {
    @Bean
    public Tesseract tesseract(Tess4jProperties tess4jProperties){
        Tesseract tesseract = new Tesseract();
        tesseract.setDatapath(tess4jProperties.getDataPath());
        tesseract.setLanguage(tess4jProperties.getLanguage());
        return tesseract;
    }
}
