package com.heima.wemedia.interceptor;

import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.thread.WmThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/14 21:29:57
 */
@Slf4j
public class WmTokenInterceptor implements HandlerInterceptor {
    /**
     * @author 18297
     * @date 2024/1/14 21:31
     * @description 前置方法，获取header中的用户信息，并存入到当前线程中
     * @param request:
     * @param response:
     * @param handler:
     * @return boolean
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取请求头中的userId
        String userId = request.getHeader("userId");
        if (userId != null) {
            // 存入到当前线程中
            WmUser wmUser = new WmUser();
            wmUser.setId(Integer.valueOf(userId));
            WmThreadLocalUtil.setUser(wmUser);
            log.info("wmTokenFilter设置用户信息到threadLocal中...");
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("清理threadLocal...");
        WmThreadLocalUtil.clear();
    }
}
