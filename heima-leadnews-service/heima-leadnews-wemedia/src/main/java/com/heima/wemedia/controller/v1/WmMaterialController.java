package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/14 21:49:58
 */
@RestController
@RequestMapping("/api/v1/material")
public class WmMaterialController {
    @Autowired
    private WmMaterialService wmMaterialService;

    /**
     * @param multipartFile:
     * @return ResponseResult<?>
     * @author 18297
     * @date 2024/1/14 21:52
     * @description 上传图片功能
     */
    @PostMapping("/upload_picture")
    public ResponseResult<?> uploadPicture(MultipartFile multipartFile) {
        return wmMaterialService.uploadPicture(multipartFile);
    }

    /**
     * @param dto:
     * @return ResponseResult
     * @author 18297
     * @date 2024/1/14 22:58
     * @description 查看素材列表
     */
    @PostMapping("/list")
    public ResponseResult<?> findList(@RequestBody WmMaterialDto dto) {
        return wmMaterialService.findList(dto);
    }

    /**
     * @author 18297
     * @date 2024/1/20 17:31
     * @description 删除图片
     * @param id:
     * @return ResponseResult<?>
     */
    @GetMapping("/del_picture/{id}")
    public ResponseResult<?> deletePicture(@PathVariable Integer id){
        return wmMaterialService.deletePicture(id);
    }

    /**
     * @author 18297
     * @date 2024/1/20 17:32
     * @description 收藏图片
     * @param id:
     * @return ResponseResult<?>
     */
    @GetMapping("/collect/{id}")
    public ResponseResult<?> Collect(@PathVariable Integer id){
        return wmMaterialService.Collect(id);
    }

    /**
     * @author 18297
     * @date 2024/1/20 17:32
     * @description 取消收藏
     * @param id:
     * @return ResponseResult<?>
     */
    @GetMapping("/cancel_collect/{id}")
    public ResponseResult<?> cancelCollect(@PathVariable Integer id){
        return wmMaterialService.cancelCollect(id);
    }
}
