package com.heima.wemedia;

import com.baidu.aip.contentcensor.AipContentCensor;
import com.baidu.aip.util.Util;
import com.heima.wemedia.service.BaiduScanService;
import com.heima.wemedia.service.impl.BaiduScanServiceImpl;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

/**
 * @author liudo
 * @version 1.0
 * @project heima-leadnews
 * @date 2023/11/9 14:23:27
 */
@SpringBootTest
public class WemediaApplicationTest {

    @Autowired
    private AipContentCensor client;
    @Autowired
    private BaiduScanService baiduScanService;

    @Test
    void testGreenImage() throws IOException {
        // C:\Users\liudo\Desktop\Snipaste_2023-11-09_14-56-50.png
        String filePath = "d:\\cai.jpg";

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        // 调用接口
        // 读取本地图片，转换成 byte[]
        byte[] readFileByBytes = Util.readFileByBytes(filePath);
        JSONObject res = client.imageCensorUserDefined(readFileByBytes, null);
        System.out.println(res.toString());
    }

    @Test
    void testGreenText() {
        // 参数为待审核的文本字符串
        String text = "世上只有妈妈好,冰毒";
        JSONObject response = client.textCensorUserDefined(text);
        System.out.println(response.toString());
    }

    @Test
    void testText(){
        baiduScanService.scanText("我是一个好人");
    }
}
