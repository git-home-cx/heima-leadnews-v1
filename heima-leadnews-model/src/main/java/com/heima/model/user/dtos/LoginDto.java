package com.heima.model.user.dtos;

import lombok.Data;

/**
 * @author 18297
 * @version 1.0
 * @project heima-leadnews
 * @date 2024/1/11 10:58:25
 */
@Data
public class LoginDto {
    /**
     * 手机号
     */
    private String phone;
    /**
     * 密码
     */
    private String password;
}
