package com.heima.model.wemedia.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/14 22:55:04
 */
@Data
public class WmMaterialDto extends PageRequestDto {
    /**
     * 1.收藏
     * 0.未收藏
     */
    private Short isCollection;
}
