package com.heima.model.article.dtos;

import lombok.Data;

import java.util.Date;

/**
 * @author 18297
 * @version 1.0
 * @project heima-leadnews
 * @date 2024/1/11 22:18:46
 */
@Data
public class ArticleHomeDto {

    // 最大时间
    Date maxBehotTime;
    // 最小时间
    Date minBehotTime;
    // 分页size
    Integer size;
    // 频道ID
    String tag;
}