package com.heima.freemarker.entity;

import lombok.Data;

import java.util.Date;

/**
 * 测试用实体
 *
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/14 10:10:18
 */
@Data
public class Student {
    private String name;//姓名
    private int age;//年龄
    private Date birthday;//生日
    private Float money;//钱包
}