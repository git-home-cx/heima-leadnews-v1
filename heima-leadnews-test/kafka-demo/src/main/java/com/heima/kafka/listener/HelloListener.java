package com.heima.kafka.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/20 21:51:02
 */
@Component
public class HelloListener {
    @KafkaListener(topics = "itcast-topic")
    public void onMessage(String message) {
        if (!StringUtils.isEmpty(message)) {
            System.out.println(message);
        }
    }
}
