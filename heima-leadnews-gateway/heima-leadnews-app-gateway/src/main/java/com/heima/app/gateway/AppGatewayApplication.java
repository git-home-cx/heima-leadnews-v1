package com.heima.app.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * app网关启动类
 *
 * @author luoTao
 * @project heima-leadnews
 * @date 2024/1/13 10:06:31
 */
@SpringBootApplication
@EnableDiscoveryClient //开启注册中心
public class AppGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppGatewayApplication.class,args);
    }
}
